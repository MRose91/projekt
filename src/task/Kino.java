package task;

import java.util.Scanner;

public class Kino {


	public static void main(String[] args) {

		String reservieren;

		int option = 0;

		Scanner sc = new Scanner(System.in);

		System.out.print("Aus wie vielen Reihen besteht das Kino? Bitte geben Sie die Anzahl ein: ");

		int reihen = sc.nextInt();

		System.out.print("Aus wie vielen Pl�tzen besteht eine Reihe? Bitte geben Sie die Anzahl ein: ");

		int plaetze = sc.nextInt();

		String[][] kino = erzeugeSaal(reihen, plaetze);

		anzeigenSaal(erzeugeSaal(reihen, plaetze));

		while (option != 3) {

			System.out.println(
					"Was wollen Sie tun?  1: Platz reservieren, 2: Reservierung stornieren, 3: Programm beenden");

			option = sc.nextInt();

			if (option == 1) {
				System.out.print("Soll ein Platz reserviert werden? J/N ");

				reservieren = sc.next();

				if (reservieren.equals("J") || reservieren.equals("j")) {

					System.out.println("Bitte geben Sie eine Reihe an: ");

					int resReihe = sc.nextInt();

					System.out.println("Bitte geben Sie einen Platz an: ");

					int resPlatz = sc.nextInt();

					if (kino.length == 0) {

						anzeigenSaal(reservierenPlatz(resReihe, resPlatz, erzeugeSaal(reihen, plaetze)));
					} else {
						anzeigenSaal(reservierenPlatz(resReihe, resPlatz, kino));
					}

				}

			} else if (option == 2) {
				System.out.println("M�chten Sie einen Platz stornieren? J/N ");

				String stornieren = sc.next();

				if (stornieren.equals("J") || stornieren.equals("j")) {

					System.out.println("Bitte geben Sie die Reihe an: ");

					int stoReihe = sc.nextInt();

					System.out.println("Bitte geben Sie den Platz an: ");

					int stoPlatz = sc.nextInt();

					anzeigenSaal(stornierenPlatz(stoReihe, stoPlatz, kino));
				}

			} else {
				continue;
			}

		}
		;
	}

	private static String[][] erzeugeSaal(int reihen, int plaetze) {

		String[][] saal = new String[reihen][plaetze];

		for (int i = 0; i < reihen; i++) {

			for (int j = 0; j < plaetze; j++) {

				saal[i][j] = "O";

			}
		}
		return saal;

	}

	private static String anzeigenSaal(String[][] saal) {

		String anzeige = "";

		for (int i = 0; i < saal.length; i++) {

			for (int j = 0; j < saal[0].length; j++) {

				anzeige = anzeige + saal[i][j];

			}
			anzeige = anzeige + "\n";
		}
		System.out.println(anzeige);

		return anzeige;
	}

	private static String[][] reservierenPlatz(int resReihe, int resPlatz, String[][] saal) {

		if (saal[resReihe - 1][resPlatz - 1].equals("O")) {
			saal[resReihe - 1][resPlatz - 1] = "X";
		} else {
			System.out.println("Dieser Platz ist bereits belegt!");
		}

		return saal;

	}

	private static String[][] stornierenPlatz(int stoReihe, int stoPlatz, String[][] saal) {

		saal[stoReihe - 1][stoPlatz - 1] = "O";

		return saal;

	}

}
